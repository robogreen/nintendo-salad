""" NINTENDO SALAD.

I hope you enjoy!

Made by Rob Green.
"""

from pathlib import Path
from typing import List
from random import randint

SOURCE_FILE = Path(__file__).parent.joinpath("games.txt")


def get_salad_ingredients(game_source: Path) -> List[str]:

    if not game_source.exists():
        raise FileNotFoundError(f"Can't find games file: {game_source}")

    title_words: List[str] = []

    # Read each line of the game title file.
    with open(game_source, "r") as source_file:
        lines = source_file.readlines()

    # Break apart into delicious title chunks
    for line in lines:
        for word in line.split(" "):
            if word.strip() not in title_words:
                title_words.append(word.strip())

    return title_words


def make_salad(word_count: int, ingredients: List[str]) -> str:
    words = []
    for _ in range(word_count):

        # random word selection
        index = randint(0, len(ingredients) - 1)
        words.append(ingredients[index])

    return " ".join(words)


if __name__ == "__main__":

    print(
        """

     _______  .__        __                     .___                      .__              .___
     \      \ |__| _____/  |_  ____   ____    __| _/____     ___________  |  | _____     __| _/
     /   |   \|  |/    \   __\/ __ \ /    \  / __ |/  _ \   /  ___/\__  \ |  | \__  \   / __ | 
    /    |    \  |   |  \  | \  ___/|   |  \/ /_/ (  <_> )  \___ \  / __ \|  |__/ __ \_/ /_/ | 
    \____|__  /__|___|  /__|  \___  >___|  /\____ |\____/  /____  >(____  /____(____  /\____ | 
            \/        \/          \/     \/      \/             \/      \/          \/      \/

"""
    )

    print("\nRandom game name generator inspired by Game Scoop ep. 768, enjoy!\n")

    ingredients = get_salad_ingredients(game_source=SOURCE_FILE)
    error_msg = "!*!*! ERROR: Value must be a whole number between 1 and 10 !*!*!\n"

    while True:
        try:
            response = input("Type the number of words to generate (1-10) and hit Enter: ")
            count = int(response)
            if count not in range(1, 11):
                print(error_msg)
                continue
            print(make_salad(word_count=count, ingredients=ingredients), "\n")
        except ValueError:
            print(error_msg)
        except KeyboardInterrupt:
            print("The fun is over...")
            quit()

