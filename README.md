# Nintendo Salad

This a super simple word salad game for making random game names. Inspired by Game Scoop. To run the game - make sure you have python 3.10 or later installed (no other dependencies needed) and run: 
```
python salad.py
```